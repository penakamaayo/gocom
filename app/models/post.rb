# == Schema Information
#
# Table name: posts
#
#  id         :integer          not null, primary key
#  title      :string
#  content    :text
#  admin_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_posts_on_admin_id  (admin_id)
#

class Post < ActiveRecord::Base
  belongs_to :user

  scope :latest, -> { order(:created_at => :desc) }
  scope :oldest, -> { order(:created_at => :asc) }
  scope :recently_updated, -> { order(:updated_at => :desc) }

  paginates_per 6
end
