class ContactMailer < ActionMailer::Base

  default :from =>  'no-reply@gocomsystems.net'


  def new_contact contact
    @contact = contact

    mail :to => 'penakamaayo@gmail.com', :subject => ContactForm::CONTACT_OPTIONS.key(@contact.subject)
  end


end
