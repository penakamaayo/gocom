class ContactsController < ApplicationController


  def create
    @contact_form = ContactForm.new params


    if @contact_form.validate(params)
      ContactMailer.new_contact(@contact_form).deliver_now

      flash[:notice] = 'We appreciate that you’ve taken the time to write us. We’ll get back to you very soon. Please come back and see us often.'
      redirect_to :back
    else
      render :error
    end
  end


end
