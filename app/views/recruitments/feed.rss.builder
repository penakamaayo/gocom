#encoding: UTF-8

xml.instruct! :xml, :version => "1.0"
xml.rss :version => "2.0" do
  xml.channel do
    xml.title 'GOCOM Recruitment Posts'
    xml.author 'GOCOM Systems and Solutions Corporation'
    xml.description 'GOCOM Systems and Solutions Corporation is an Information and Communications Technology company that focuses on Solutions and Services'
    xml.link 'https://gocomsystems.herokuapp.com/'
    xml.language "en"

    for post in @posts
      xml.item do
        if post.title
          xml.title post.title
        else
          xml.title ""
        end
        xml.author post.author
        xml.pubDate post.date_posted
        xml.link 'https://gocomsystems.herokuapp.com/recruitments' + post.id.to_s
        xml.guid post.id

        text = post.content.html_safe

        xml.description "<p>" + text + "</p>"

      end
    end
  end
end
