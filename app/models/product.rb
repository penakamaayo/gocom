# == Schema Information
#
# Table name: products
#
#  id                  :integer          not null, primary key
#  name                :string
#  label               :string
#  description         :text
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  product_category_id :integer
#  image               :string
#  link                :string
#
# Indexes
#
#  index_products_on_product_category_id  (product_category_id)
#

class Product < ActiveRecord::Base
  belongs_to :product_category

  scope :latest, -> { order(:created_at => :desc) }
end
