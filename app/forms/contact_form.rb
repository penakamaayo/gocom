class ContactForm < Reform::Form

  CONTACT_OPTIONS = {
    "Technical Support and Presales" => "support@gocom.ph",
    "Sales and Marketing" => "sales@gocom.ph",
    "Recruitment" => "recruitment@gocom.ph"
  }


  property :name, :virtual => true
  property :email, :virtual => true
  property :subject, :virtual => true
  property :message, :virtual => true


  validates :name, :email, :subject, :message, :presence => true

end
