class PostDecorator < ApplicationDecorator

  delegate_all


  def author
    return unless source.admin_id

    author = Admin.find_by_id source.admin_id
    author.name
  end


  def date_posted
    source.created_at.localtime.strftime '%b %d, %Y, %l:%M %p'
  end


  def date_updated
    source.updated_at.localtime.strftime '%b %d, %Y, %l:%M %p'
  end

end
