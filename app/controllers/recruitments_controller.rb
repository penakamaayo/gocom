class RecruitmentsController < ApplicationController

  def index
    @posts = PostDecorator.decorate_collection Post.oldest.page(params[:page])
  end


  def show
    @post = Post.find(params[:id]).decorate
  end


  def feed
    @posts = PostDecorator.decorate_collection Post.recently_updated

    respond_to do |format|
      format.rss { render :layout => false }
    end
  end
end
