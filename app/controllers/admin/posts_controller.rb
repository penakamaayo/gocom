class Admin::PostsController < AdminBaseController

  before_filter :fetch_post, :only => [:show, :edit, :update, :destroy]


  def index
    @posts = PostDecorator.decorate_collection(Post.latest.page(params[:page]).per 10)
  end

  def show
    @post =  @post.decorate
  end


  def new
    @form = PostForm.new Post.new
  end


  def create
    @form = PostForm.new Post.new

    if @form.validate(post_params) && @form.save
      flash[:notice] = 'Successfully created post!'
      redirect_to [:admin, :posts]
    else
      render :new
    end


  end


  def edit
    @form = PostForm.new @post
  end


  def update
    @form = PostForm.new @post

    if @form.validate(post_params) && @form.save
      flash[:notice] = 'Successfully updated post!'
      redirect_to [:edit, :admin, @post]
    else
      render :edit
    end
  end


  def destroy
    @post.destroy ?
      flash[:notice] = 'Post has been deleted' :
      flash[:notice] = 'Failed to delete post'

    redirect_to [:admin, :posts]
  end





  private

  def post_params
    params.require(:post).permit :title, :content, :admin_id
  end

  def fetch_post
    @post = Post.find params[:id]
  end

end
