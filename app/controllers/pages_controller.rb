class PagesController < ApplicationController

  def home
  end

  def products
    @security_products = ProductCategory.find_by_name('Security').try :products
    @network_products = ProductCategory.find_by_name('Network').try :products
    @management_products = ProductCategory.find_by_name('Management & Monitoring').try :products
    @datacenter_products = ProductCategory.find_by_name('Datacenter, Backup & Recovery').try :products
    @storage_products = ProductCategory.find_by_name('Storage & Endpoint').try :products
  end


  def solutions
  end


  def contact_us
  end


  def about_us
  end

end
