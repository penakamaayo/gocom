Rails.application.routes.draw do

  devise_for :admins
  root 'pages#home'

  get '/about-us' => 'pages#about_us'
  get '/products' => 'pages#products'
  get '/solutions' => 'pages#solutions'
  get '/contact-us' => 'pages#contact_us'

  get '/admin' => 'admin/posts#index'

  resources :contacts, :only => :create
  resources :recruitments, :only => [:index, :show]

  get '/feed' => 'recruitments#feed'

  namespace :admin do
    resources :dashboard, :only => :index
    resources :posts
    resources :products
  end



end
