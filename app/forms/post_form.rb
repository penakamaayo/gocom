class PostForm < Reform::Form

  property :title
  property :content
  property :admin_id


  validates :title, :content, :admin_id, :presence => true

end
