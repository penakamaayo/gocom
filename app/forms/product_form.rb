class ProductForm < Reform::Form

  property :product_category_id
  property :name
  property :label
  property :description
  property :image
  property :link


  validates :name, :label, :description, :product_category_id, :link, :presence => true

end
