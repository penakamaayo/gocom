class Admin::ProductsController < AdminBaseController

  before_filter :fetch_product, :only => [:show, :edit, :update, :destroy]


  def index
    @products = Product.latest.page(params[:page]).per 10
  end

  def show
  end


  def new
    @form = ProductForm.new Product.new
  end


  def create
    @form = ProductForm.new Product.new

    if @form.validate(product_params) && @form.save
      flash[:notice] = 'Successfully created product!'
      redirect_to [:admin, :products]
    else
      render :new
    end


  end


  def edit
    @form = ProductForm.new @product
  end


  def update
    @form = ProductForm.new @product

    if @form.validate(product_params) && @form.save
      flash[:notice] = 'Successfully updated product!'
      redirect_to [:admin, @product]
    else
      render :edit
    end
  end


  def destroy
    @product.destroy ?
      flash[:notice] = 'Product has been deleted' :
      flash[:notice] = 'Failed to delete product'

    redirect_to [:admin, :products]
  end





  private

  def product_params
    params.require(:product).permit :name, :label, :description, :product_category_id, :image, :link
  end

  def fetch_product
    @product = Product.find params[:id]
  end

end
